﻿# Worker Project Guide

## Project Guide
- This is a dotnet 5 worker service project.
- Use worker to create Windows Service on Windows and Daemon Process on Linux.
- Use Quartz.net to create scheduling jobs.

## Create
```
dotnet new worker -o yourprojectname
```

## Dependencies
- Serilog.*, A Serilog sink that writes log events to the console/terminal
- Quartz.Extensions.Hosting : 3.2.4, Quartz.NET Generic Host integration; Quartz Scheduling Framework for .NET
- Microsoft.Extensions.Hosting.WindowsServices : 5.0.0, .NET hosting infrastructure for Windows Services

## Deploy
- publish: dotnet publish -c Release -o yourabsolutepathfolder
- create windows service: C:\Windows\System32\sc create YourServiceName binPath= "yourabsolutepathfolder\YourProject.exe" type= own start= auto 
- start windows service: C:\Windows\System32\sc start YourServiceName
- stop windows service: C:\Windows\System32\sc stop YourServiceName
- delete windows service: C:\Windows\System32\sc delete YourServiceName
