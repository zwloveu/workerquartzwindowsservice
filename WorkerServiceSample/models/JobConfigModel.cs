using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerServiceSample.Models
{
    public class JobConfigModel
    {
        public string JobKey { get; set; }

        public string JobGroup { get; set; }

        public string JobDesc { get; set; }

        public string JobTrgIdName { get; set; }

        public string JobTrgIdDesc { get; set; }
    }
}
