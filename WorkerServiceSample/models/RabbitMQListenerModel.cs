using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerServiceSample.Models
{
    public class RabbitMQListenerModel
    {
        public string ExchangeName { get; set; }
        public string ExchangeType { get; set; }
        public string QueueName { get; set; }
        public string RouteKey { get; set; }
        public bool UseDLX { get; set; }
        public bool ListenDLX { get; set; }
        public int ConsumerCount { get; set; }
    }

    public class SaveQueueMsgWorkerListenerModel : RabbitMQListenerModel { }

    public class DQSaveQueueMsgWorkerListenerModel : RabbitMQListenerModel { }
}
