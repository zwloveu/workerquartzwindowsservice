using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerServiceSample.Models
{
    public class RabbitMQInstanceModel
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public ushort RequestedHeartbeat { get; set; }
        public string Endpoint { get; set; }
        public string VirtualHost { get; set; }
    }

    public class DefaultRabbitMQInstanceModel : RabbitMQInstanceModel { }
}
