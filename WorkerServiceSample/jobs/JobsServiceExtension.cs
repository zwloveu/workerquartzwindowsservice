using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Quartz;
using WorkerServiceSample.Jobs;
using WorkerServiceSample.Models;
using System;
using System.Collections.Specialized;

namespace WorkerServiceSample
{
    public static class JobsServiceExtension
    {
        public static IServiceCollection AddJobsToQuartz(this IServiceCollection services, IConfiguration configuration)
        {
            var properties = new NameValueCollection
            {
                ["quartz.scheduler.instanceName"] = "Jobs.Scheduler",
                ["quartz.threadPool.type"] = "Quartz.Simpl.SimpleThreadPool, Quartz",
                ["quartz.threadPool.threadCount"] = "5",
                ["quartz.serializer.type"] = "json",
                //warning: quartz remote scheduler with version 3.2.4 now is not supporting in dotnet core
                ["quartz.scheduler.exporter.type"] = "Quartz.Simpl.RemotingSchedulerExporter, Quartz",
                ["quartz.scheduler.exporter.port"] = "555",
                ["quartz.scheduler.exporter.bindName"] = "QuartzScheduler",
                ["quartz.scheduler.exporter.channelType"] = "tcp",
                ["quartz.scheduler.exporter.channelName"] = "httpQuartz",
                ["quartz.scheduler.exporter.rejectRemoteRequests"] = "true"
            };

            services.AddQuartz(properties, q =>
            { 
                // handy when part of cluster or you want to otherwise identify multiple schedulers
                q.SchedulerId = "Scheduler-Core";

                // this is default configuration if you don't alter it
                q.UseMicrosoftDependencyInjectionJobFactory(options =>
                {
                    // if we don't have the job in DI, allow fallback to configure via default constructor
                    options.AllowDefaultConstructor = true;

                    // set to true if you want to inject scoped services like Entity Framework's DbContext
                    options.CreateScope = false;
                });

                // these are the defaults
                q.UseSimpleTypeLoader();
                q.UseInMemoryStore();
                q.UseDefaultThreadPool(tp =>
                {
                    tp.MaxConcurrency = 1;
                });

                // configure jobs with code
                var jobConfigModel = new JobConfigModel();
                configuration.GetSection("JobSettings:SimpleProducerJob").Bind(jobConfigModel);
                var jobKey = new JobKey(jobConfigModel.JobKey, jobConfigModel.JobGroup);
                q.ScheduleJob<SimpleProducerJob>(
                    trigger => trigger
                        .WithIdentity(jobConfigModel.JobTrgIdName)
                        .ForJob(jobKey)
                        .StartNow()
                        .WithSimpleSchedule(x => x.WithInterval(TimeSpan.FromSeconds(2)).RepeatForever())
                        .WithDescription(jobConfigModel.JobTrgIdDesc),
                    job => job
                        .StoreDurably()
                        .WithIdentity(jobKey)
                        .WithDescription(jobConfigModel.JobDesc)
                 );
            });

            return services;
        }
    }
}
