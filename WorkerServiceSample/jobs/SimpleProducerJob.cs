using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Quartz;

namespace WorkerServiceSample.Jobs
{
    public class SimpleProducerJob : IJob, IDisposable
    {
        private readonly ILogger<SimpleProducerJob> _Logger;
        private readonly RabbitMQClient _RabbitMQClient;
        private string _JobFullIdentity;

        public SimpleProducerJob(ILogger<SimpleProducerJob> logger, RabbitMQClient rabbitMQClient)
        {
            this._Logger = logger?? throw new ArgumentNullException(nameof(logger));
            this._RabbitMQClient = rabbitMQClient?? throw new ArgumentNullException(nameof(rabbitMQClient));
        }

        public async Task Execute(IJobExecutionContext context)
        {         
            _JobFullIdentity = $"{context.FireInstanceId}:{context.JobDetail.Key}]:[{context.Trigger.Key}";

            _Logger.LogInformation($"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] SimpleProducerJob [{_JobFullIdentity}] Execute Begin");
            
            _RabbitMQClient.PushMessage(Guid.NewGuid().ToString());

            _Logger.LogInformation($"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] SimpleProducerJob [{_JobFullIdentity}] Execute End");

            await Task.CompletedTask;
        }

        public void Dispose()
        {
            _Logger.LogInformation($"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] SimpleProducerJob [{_JobFullIdentity}] Dispose");
        }
    }
}