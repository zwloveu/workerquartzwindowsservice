using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Quartz;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using WorkerServiceSample;

CreateHostBuilder(args).Build().Run();

IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .UseSerilog((builderContext, config) =>
        {
            config.ReadFrom.Configuration(new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("serilog.json", optional: false, reloadOnChange: false)
                .Build());
        })
        .UseWindowsService()
        .ConfigureLogging(loggerFactory => loggerFactory.AddEventLog())
        .ConfigureServices((hostContext, services) =>
        {
            services.AddWorkers(hostContext.Configuration);

            services
                .AddJobsToQuartz(hostContext.Configuration)
                .AddQuartzHostedService(options =>
                {
                    options.WaitForJobsToComplete = true;
                });

                services.AddSingleton<RabbitMQClient>();
        });


