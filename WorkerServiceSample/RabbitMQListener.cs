using WorkerServiceSample.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WorkerServiceSample
{
    public abstract class RabbitMQListener : BackgroundService
    {
        protected readonly ILogger<RabbitMQListener> _Logger;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;
        private readonly RabbitMQInstanceModel _RabbitMQInstanceModel;
        private readonly RabbitMQListenerModel _RabbitMQListenerModel;
        private readonly string _DlxRouteKey;
        private readonly string _DlxQueueName;
        private readonly string _DlxExchangeName;

        public RabbitMQListener(RabbitMQInstanceModel rmqInstance,
            RabbitMQListenerModel rmqListener,
            ILogger<RabbitMQListener> logger)
        {
            this._Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._RabbitMQInstanceModel = rmqInstance ?? throw new ArgumentNullException(nameof(rmqInstance));
            this._RabbitMQListenerModel = rmqListener ?? throw new ArgumentNullException(nameof(rmqListener));

            this._DlxRouteKey = $"D{_RabbitMQListenerModel.RouteKey}#";
            this._DlxQueueName = $"D{_RabbitMQListenerModel.QueueName}";
            this._DlxExchangeName = $"D{_RabbitMQListenerModel.ExchangeName}";

            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = _RabbitMQInstanceModel.HostName,
                    UserName = _RabbitMQInstanceModel.UserName,
                    Password = _RabbitMQInstanceModel.Password,
                    Port = _RabbitMQInstanceModel.Port,
                    VirtualHost = _RabbitMQInstanceModel.VirtualHost,
                    DispatchConsumersAsync = true,
                    ConsumerDispatchConcurrency = _RabbitMQListenerModel.ConsumerCount,
                };
                this._Connection = factory.CreateConnection();
                this._Channel = _Connection.CreateModel();
            }
            catch (Exception ex)
            {
                this._Logger.LogError(ex, $"RabbitListener init failed");
            }
        }

        public abstract Task<bool> Process(string message);

        private async Task Register(int consumerId)
        {
            _Logger.LogInformation($"RabbitListener register,routeKey:{_RabbitMQListenerModel.RouteKey}-{consumerId}");
            _Channel.ExchangeDeclare(exchange: _RabbitMQListenerModel.ExchangeName,
                type: _RabbitMQListenerModel.ExchangeType,
                durable: true, autoDelete: false);
            _Channel.QueueDeclare(queue: _RabbitMQListenerModel.QueueName, exclusive: false, durable: true, autoDelete: false,
                arguments: _RabbitMQListenerModel.UseDLX ? new Dictionary<string, object>
                              {
                                  { "x-dead-letter-exchange", _DlxExchangeName },
                                  { "x-dead-letter-routing-key", _DlxRouteKey },
                              } : null);
            _Channel.QueueBind(queue: _RabbitMQListenerModel.QueueName,
                              exchange: _RabbitMQListenerModel.ExchangeName,
                              routingKey: _RabbitMQListenerModel.RouteKey);

            // bind dlx
            if (_RabbitMQListenerModel.UseDLX)
            {
                _Channel.ExchangeDeclare(exchange: _DlxExchangeName,
                    type: _RabbitMQListenerModel.ExchangeType,
                    durable: true, autoDelete: false);
                _Channel.QueueDeclare(queue: _DlxQueueName, exclusive: false, durable: true, autoDelete: false);
                _Channel.QueueBind(queue: _DlxQueueName,
                                  exchange: _DlxExchangeName,
                                  routingKey: _DlxRouteKey);
            }

            _Channel.BasicQos(0, 1, false);
            var consumer = new AsyncEventingBasicConsumer(_Channel);
            consumer.Received += async (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var result = await Process(message);
                if (result)
                {
                    _Channel.BasicAck(ea.DeliveryTag, false);
                }
                else
                {
                    if (_RabbitMQListenerModel.UseDLX)
                    {
                        _Channel.BasicNack(ea.DeliveryTag, false, false);
                    }
                }
            };
            await consumer.HandleBasicConsumeOk($"{consumerId}");
            _Channel.BasicConsume(queue: _RabbitMQListenerModel.QueueName, consumer: consumer, autoAck: false);
        }

        private void DeRegister()
        {
            _Connection.Close();
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            for (var index = 0; index < _RabbitMQListenerModel.ConsumerCount; index++)
            {
                await Register(index + 1);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            DeRegister();
            return Task.CompletedTask;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
            }
        }

    }
}
