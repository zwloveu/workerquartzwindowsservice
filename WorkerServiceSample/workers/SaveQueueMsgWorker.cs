using WorkerServiceSample.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using Dapper;
using Microsoft.Data.SqlClient;
using System.IO;
using System.Data;
using System.Threading.Tasks;

namespace WorkerServiceSample.Workers
{
    public class SaveQueueMsgWorker : RabbitMQListener
    {
        private readonly IConfiguration _Configuration;

        public SaveQueueMsgWorker(DefaultRabbitMQInstanceModel rmqInstance,
            SaveQueueMsgWorkerListenerModel rmqListener,
            ILogger<SaveQueueMsgWorker> logger,
            IConfiguration configration) : base(rmqInstance, rmqListener, logger)
        {
            this._Configuration = configration ?? throw new ArgumentNullException(nameof(configration));
        }

        public override async Task<bool> Process(string message)
        {
            _Logger.LogInformation($"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] SaveQueueMsgWorker Listener Handle Begin");

            var fileIdentity = $"{DateTimeOffset.Now:yyyyMMddhhmmssffffff}";
            var resultFlag = SaveFile(fileIdentity, message);
            // if (resultFlag) resultFlag = await SaveProcessIdentity(fileIdentity);

            _Logger.LogInformation($"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] SaveQueueMsgWorker Listener Handle End");

            return await Task.FromResult(true);
        }

        private bool SaveFile(string fileIdentity, string message)
        {
            try
            {
                var fi = new System.IO.FileInfo(_Configuration["SaveQueueMsgWorkerFilePath"]);
                var filePath = Path.Combine(fi.DirectoryName, $"{fi.Name.Split(".")[0]}_{fileIdentity}{fi.Extension}");
                using var fst = new FileStream(filePath, FileMode.OpenOrCreate);
                using var swt = new StreamWriter(fst, System.Text.Encoding.GetEncoding("utf-8"));
                swt.WriteLine(message);
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, $"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] SaveQueueMsgWorker Listener Save File Failed");
                return false;
            }

            return true;
        }

        private async Task<bool> SaveProcessIdentity(string fileIdentity)
        {
            var affectedRows = 0;
            var sql = "INSERT INTO dbo.TALBEXX (FileIdentity) VALUES (@FileIdentity)";
            try
            {
                using var connection = new SqlConnection(_Configuration.GetConnectionString("TestDb"));
                connection.Open();

                affectedRows = await connection.ExecuteAsync(
                    sql,
                    new { FileIdentity = fileIdentity },
                    commandType: CommandType.Text);

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, $"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] SaveQueueMsgWorker Listener Save Process Identity Failed");
            }

            return affectedRows > 0;
        }
    }
}
