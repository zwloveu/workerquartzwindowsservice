using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Quartz;
using WorkerServiceSample.Jobs;
using WorkerServiceSample.Models;
using System;
using System.Collections.Specialized;
using WorkerServiceSample.Workers;

namespace WorkerServiceSample
{
    public static class WorkerServiceExtension
    {
        public static IServiceCollection AddWorkers(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .ConfigurePOCO<DefaultRabbitMQInstanceModel>(configuration.GetSection("RabbitMQInstances:Default"), () => new DefaultRabbitMQInstanceModel());

            var saveQueueMsgWorkerListenerModel = services.ConfigurePOCO<SaveQueueMsgWorkerListenerModel>(configuration.GetSection("RabbitMQListeners:SaveQueueMsgServiceListener"), () => new SaveQueueMsgWorkerListenerModel());
            services
                .AddHostedService<SaveQueueMsgWorker>();

            if (saveQueueMsgWorkerListenerModel.UseDLX == true)
            {
                services
                    .ConfigurePOCO<DQSaveQueueMsgWorkerListenerModel>(configuration.GetSection("RabbitMQListeners:DQBillAnalysisServiceListener"), () => new DQSaveQueueMsgWorkerListenerModel());
                services.AddHostedService<DQSaveQueueMsgWorker>();
            }

            return services;
        }
    }
}
