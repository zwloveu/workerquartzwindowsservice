using WorkerServiceSample.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using Dapper;
using Microsoft.Data.SqlClient;
using System.IO;
using System.Data;
using System.Threading.Tasks;

namespace WorkerServiceSample.Workers
{
    public class DQSaveQueueMsgWorker : RabbitMQListener
    {
        private readonly IConfiguration _Configuration;

        public DQSaveQueueMsgWorker(DefaultRabbitMQInstanceModel rmqInstance,
            DQSaveQueueMsgWorkerListenerModel rmqListener,
            ILogger<DQSaveQueueMsgWorker> logger,
            IConfiguration configration) : base(rmqInstance, rmqListener, logger)
        {
            this._Configuration = configration ?? throw new ArgumentNullException(nameof(configration));
        }

        public override async Task<bool> Process(string message)
        {
            _Logger.LogInformation($"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] DQSaveQueueMsgWorker Listener Handle Begin");

            // todo: here do your business logic
            await Task.Delay(1000);

            _Logger.LogInformation($"[{ DateTimeOffset.Now:yyyy-MM-dd hh:mm:ss fffffff}] DQSaveQueueMsgWorker Listener Handle End");

            return true;
        }
    }
}
