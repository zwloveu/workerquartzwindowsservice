using WorkerServiceSample.Models;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerServiceSample
{
    public class RabbitMQClient
    {
        private readonly ILogger<RabbitMQClient> _Logger;
        private readonly IConnection _Connection;
        private readonly RabbitMQInstanceModel _RabbitMQInstanceModel;
        private readonly RabbitMQListenerModel _RabbitMQListenerModel;
        private readonly string _DlxRouteKey;
        private readonly string _DlxExchangeName;

        public RabbitMQClient(DefaultRabbitMQInstanceModel rmqInstance,
            SaveQueueMsgWorkerListenerModel rmqListener,
            ILogger<RabbitMQClient> logger)
        {
            this._Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._RabbitMQInstanceModel = rmqInstance ?? throw new ArgumentNullException(nameof(rmqInstance));
            this._RabbitMQListenerModel = rmqListener ?? throw new ArgumentNullException(nameof(rmqListener));

            this._DlxRouteKey = $"D{_RabbitMQListenerModel.RouteKey}#";
            this._DlxExchangeName = $"D{_RabbitMQListenerModel.ExchangeName}";

            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = _RabbitMQInstanceModel.HostName,
                    UserName = _RabbitMQInstanceModel.UserName,
                    Password = _RabbitMQInstanceModel.Password,
                    Port = _RabbitMQInstanceModel.Port,
                    VirtualHost = _RabbitMQInstanceModel.VirtualHost,
                };
                this._Connection = factory.CreateConnection();
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, "RabbitMQClient init failed");
            }
        }

        public virtual void PushMessage(string message)
        {
            _Logger.LogInformation($"PushMessage,routingKey:{_RabbitMQListenerModel.RouteKey}");

            using var channel = _Connection.CreateModel();
            channel.ExchangeDeclare(exchange: _RabbitMQListenerModel.ExchangeName,
                type: _RabbitMQListenerModel.ExchangeType,
                durable: true, autoDelete: false);
            channel.QueueDeclare(queue: _RabbitMQListenerModel.QueueName, exclusive: false, durable: true, autoDelete: false,
                arguments: _RabbitMQListenerModel.UseDLX ? new Dictionary<string, object>
                              {
                                  { "x-dead-letter-exchange", _DlxExchangeName },
                                  { "x-dead-letter-routing-key", _DlxRouteKey },
                              } : null);
            channel.QueueBind(queue: _RabbitMQListenerModel.QueueName,
                              exchange: _RabbitMQListenerModel.ExchangeName,
                              routingKey: _RabbitMQListenerModel.RouteKey);

            channel.ConfirmSelect();
            IBasicProperties properties = channel.CreateBasicProperties();
            properties.DeliveryMode = 2;

            channel.BasicPublish(exchange: _RabbitMQListenerModel.ExchangeName,
                                    routingKey: _RabbitMQListenerModel.RouteKey,
                                    basicProperties: properties,
                                    body: Encoding.UTF8.GetBytes(message));
            channel.WaitForConfirms();
        }
    }

}
